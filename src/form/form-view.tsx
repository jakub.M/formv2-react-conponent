import React, { BaseSyntheticEvent, FC, ReactNode } from 'react';
import { UseFormMethods, FormProvider } from 'react-hook-form';
import { LoadingBackDrop } from '../backdrop/loading-back-drop';

export const FormView: FC<Props> = ({ onSubmit, methods, children, loading }) => (
  <FormProvider {...methods}>
    <form {...{ onSubmit }}>{children}</form>
    <LoadingBackDrop {...{ loading }} />
  </FormProvider>
);

interface Props {
  onSubmit: (e?: BaseSyntheticEvent<object, any, any> | undefined) => Promise<void>;
  methods: UseFormMethods<Record<string, any>>;
  children: ReactNode;
  loading: boolean;
}
