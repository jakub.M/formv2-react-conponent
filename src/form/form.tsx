import React, { FC, useEffect } from 'react';
import { Validation } from 'itf-validation';
import { yupResolver } from '@hookform/resolvers';
import { AxiosResponse } from 'axios';
import { FormView } from './form-view';
import { useApi } from '../api-service/useApi';
import { MsgResult } from '../api-service/msg-result';


export const Form: FC<Props> = ({ metatype, apiProps, children, editId }) => {
  const { api, loading, methods } = useApi(createUseFormSets(metatype));
  const onSubmit = methods.handleSubmit((data: any) => {
    api({
      method: 'post',
      path: Validation.getFormPath(metatype, 'post'),
      data,
      ...apiProps,
    } as any);
  });

  useEffect(() => {
    if (editId) {
      api({ method: 'get', path: Validation.getFormPath(metatype, 'edit'), onSuccess: methods.reset });
    }
  }, [])

  return <FormView {...{ methods, onSubmit, children, loading }} />;
};

const createUseFormSets = (metatype: any) => {
  const validationSchema = Validation.getSchema(metatype);
  return { resolver: yupResolver(validationSchema as any) };
};

interface Props {
  apiProps: ApiProps;
  metatype: any;
  editId?: string | undefined;
}

interface ApiProps {
  msgResult?: MsgResult;
  onSuccess: (res?: AxiosResponse<any>) => void;
}
