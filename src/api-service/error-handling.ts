import { useContext } from 'react';
import cogoToast from 'cogo-toast';
import { MsgResult } from './msg-result';
import { formContext } from '../context/form.context';

export const handlingError = (response: any, setError: any, msgResult?: MsgResult) => {
  if (!response) {
    return cogoToast.error('Brak połączenia z serwerem');
  }
  const { translatedApiErrors } = useContext(formContext);
  const { status, data } = response;
  const { message } = data;
  switch (status) {
    case 400:
      typeof message.isArray()
        ? message.map(({ path, message }: any) => setError(path, { message }))
        : showError(translatedApiErrors[message as keyof typeof translatedApiErrors] ?? message, msgResult);
      break;
    default:
      showError(translatedApiErrors[message] ?? message, msgResult);
  }
};

const showError = (error: any, msgResult?: MsgResult) => {
  if (msgResult && [MsgResult.ALWAYS, MsgResult.ON_FAILURE].includes(msgResult)) {
    cogoToast.error(error);
  }
};
