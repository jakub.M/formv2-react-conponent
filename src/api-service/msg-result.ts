export enum MsgResult {
  ON_SUCCESS,
  ON_FAILURE,
  ALWAYS,
}
