import axios, { Method } from 'axios';
import cogoToast from 'cogo-toast';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { MsgResult } from './msg-result';
import { handlingError } from './error-handling';
import { formContext } from '../context/form.context';
import { useContext } from 'react';

export const useApi = (props: any) => {
  const [loading, setLoading] = useState(false);
  const methods = useForm(props);
  const { apiUrl } = useContext(formContext);

  const api = async <T>({ method, path, msgResult, data, onSuccess, withoutLoading, withDelay }: ApiProps<T>) => {
    if (!withoutLoading) {
      setLoading(true);
    }
    const startAt = new Date();
    axios(axiosConfig(path, method, apiUrl, data))
      .then((res) =>
        setTimeout(() => {
          onSuccess?.(res?.data);
          if (msgResult && [MsgResult.ALWAYS, MsgResult.ON_SUCCESS].includes(msgResult)) {
            cogoToast.success('Sukces');
          }
          setLoading(false);
        }, getSetTimeoutTime(startAt, withDelay))
      )
      .catch((err) =>
        setTimeout(() => {
          setLoading(false);
          handlingError(err?.response, methods.setError, msgResult);
        }, getSetTimeoutTime(startAt, withDelay))
      );
  };

  return { api, loading, methods };
};

const delay = 1000;

const getSetTimeoutTime = (startAt: Date, withDelay?: boolean) => {
  const latency = startAt.getTime() - new Date().getTime();
  const isLatencyMoreThenDelay = latency < delay;
  return  withDelay && isLatencyMoreThenDelay ? delay - latency : 0;
}

const axiosConfig = (path: string, method: Method,  apiUrl: string, data?: any) => ({
  withCredentials: true,
  timeout: 4000,
  url: apiUrl + path,
  method,
  data,
});

interface ApiProps<T> {
  method: Method;
  path: string;
  msgResult?: MsgResult;
  data?: any;
  onSuccess?: (res: any) => void;
  withoutLoading?: boolean;
  withDelay?: boolean;
}
