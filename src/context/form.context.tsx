import React, { createContext, FC } from 'react';

interface FormContextInterface {
  apiUrl: string;
  translatedApiErrors: any;
}

export const formContext = createContext<FormContextInterface>({
  apiUrl: '',
  translatedApiErrors: {},
});

const {Provider} = formContext;

export const FormProvider: FC<FormContextInterface> = ({ children, ...props }) =>
<Provider value={props}>
  {children}
</Provider>