import styled from 'styled-components';

export const BackDrop: any = styled.div`
  background-color: rgba(48, 49, 48, 0.22);
  height: 100%;
  position: fixed;
  transition: all 1.3s;
  width: 100%;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: ${(props: any) => props?.loading ? 'flex' : 'none'};
  justify-content: center;
  align-items: center;
`;
