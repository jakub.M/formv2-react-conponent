import React, { FC } from 'react';
import { BackDrop } from './back-drop.styled';
import { Ellipsis } from './spinner';

export const LoadingBackDrop: FC<Props> = (props) => (
  <BackDrop {...props}>
    <Ellipsis />
  </BackDrop>
);

interface Props {
  loading: boolean;
}
