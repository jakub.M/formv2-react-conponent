var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
import styled from 'styled-components';
export var BackDrop = styled.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  background-color: rgba(48, 49, 48, 0.22);\n  height: 100%;\n  position: fixed;\n  transition: all 1.3s;\n  width: 100%;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  display: ", ";\n  justify-content: center;\n  align-items: center;\n"], ["\n  background-color: rgba(48, 49, 48, 0.22);\n  height: 100%;\n  position: fixed;\n  transition: all 1.3s;\n  width: 100%;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  display: ", ";\n  justify-content: center;\n  align-items: center;\n"])), function (props) { return (props === null || props === void 0 ? void 0 : props.loading) ? 'flex' : 'none'; });
var templateObject_1;
