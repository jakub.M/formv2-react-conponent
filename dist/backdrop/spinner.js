var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from 'react';
import styled, { keyframes } from 'styled-components';
var motion1 = function () { return keyframes(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  0% {\n    transform: scale(0);\n  }\n  100% {\n    transform: scale(1);\n  }\n"], ["\n  0% {\n    transform: scale(0);\n  }\n  100% {\n    transform: scale(1);\n  }\n"]))); };
var motion2 = function () { return keyframes(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n   0% {\n    transform: translate(0, 0);\n  }\n  100% {\n    transform: translate(19px, 0);\n  }\n"], ["\n   0% {\n    transform: translate(0, 0);\n  }\n  100% {\n    transform: translate(19px, 0);\n  }\n"]))); };
var motion3 = function () { return keyframes(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n  0% {\n    transform: scale(1);\n  }\n  100% {\n    transform: scale(0);\n  }\n"], ["\n  0% {\n    transform: scale(1);\n  }\n  100% {\n    transform: scale(0);\n  }\n"]))); };
var EllipsisSpinner = styled.div(templateObject_4 || (templateObject_4 = __makeTemplateObject(["\n  display: inline-block;\n  position: relative;\n  width: ", ";\n  height: ", ";\n  div {\n    position: absolute;\n    top: 27px;\n    width: 11px;\n    height: 11px;\n    border-radius: 50%;\n    background: ", ";\n    animation-timing-function: cubic-bezier(0, 1, 1, 0);\n  }\n  div:nth-child(1) {\n    left: 6px;\n    animation: ", " 0.6s infinite;\n  }\n  div:nth-child(2) {\n    left: 6px;\n    animation: ", " 0.6s infinite;\n  }\n  div:nth-child(3) {\n    left: 26px;\n    animation: ", " 0.6s infinite;\n  }\n  div:nth-child(4) {\n    left: 45px;\n    animation: ", " 0.6s infinite;\n  }\n"], ["\n  display: inline-block;\n  position: relative;\n  width: ", ";\n  height: ", ";\n  div {\n    position: absolute;\n    top: 27px;\n    width: 11px;\n    height: 11px;\n    border-radius: 50%;\n    background: ", ";\n    animation-timing-function: cubic-bezier(0, 1, 1, 0);\n  }\n  div:nth-child(1) {\n    left: 6px;\n    animation: ", " 0.6s infinite;\n  }\n  div:nth-child(2) {\n    left: 6px;\n    animation: ", " 0.6s infinite;\n  }\n  div:nth-child(3) {\n    left: 26px;\n    animation: ", " 0.6s infinite;\n  }\n  div:nth-child(4) {\n    left: 45px;\n    animation: ", " 0.6s infinite;\n  }\n"])), function (p) { return "" + p.size + p.sizeUnit; }, function (p) { return "" + p.size + p.sizeUnit; }, function (props) { var _a; return (_a = props.color) !== null && _a !== void 0 ? _a : props.theme.colors.secondary; }, motion1(), motion2(), motion2(), motion3());
export var Ellipsis = function (props) { return (React.createElement(EllipsisSpinner, __assign({}, props),
    React.createElement("div", null),
    React.createElement("div", null),
    React.createElement("div", null),
    React.createElement("div", null))); };
Ellipsis.defaultProps = {
    size: 64,
    sizeUnit: 'px'
};
var templateObject_1, templateObject_2, templateObject_3, templateObject_4;
