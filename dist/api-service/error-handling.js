import { useContext } from 'react';
import cogoToast from 'cogo-toast';
import { MsgResult } from './msg-result';
import { formContext } from '../context/form.context';
export var handlingError = function (response, setError, msgResult) {
    var _a, _b;
    if (!response) {
        return cogoToast.error('Brak połączenia z serwerem');
    }
    var translatedApiErrors = useContext(formContext).translatedApiErrors;
    var status = response.status, data = response.data;
    var message = data.message;
    switch (status) {
        case 400:
            typeof message.isArray()
                ? message.map(function (_a) {
                    var path = _a.path, message = _a.message;
                    return setError(path, { message: message });
                })
                : showError((_a = translatedApiErrors[message]) !== null && _a !== void 0 ? _a : message, msgResult);
            break;
        default:
            showError((_b = translatedApiErrors[message]) !== null && _b !== void 0 ? _b : message, msgResult);
    }
};
var showError = function (error, msgResult) {
    if (msgResult && [MsgResult.ALWAYS, MsgResult.ON_FAILURE].includes(msgResult)) {
        cogoToast.error(error);
    }
};
