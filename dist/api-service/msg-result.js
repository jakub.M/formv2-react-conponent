export var MsgResult;
(function (MsgResult) {
    MsgResult[MsgResult["ON_SUCCESS"] = 0] = "ON_SUCCESS";
    MsgResult[MsgResult["ON_FAILURE"] = 1] = "ON_FAILURE";
    MsgResult[MsgResult["ALWAYS"] = 2] = "ALWAYS";
})(MsgResult || (MsgResult = {}));
