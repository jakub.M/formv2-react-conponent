var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React, { useEffect } from 'react';
import { Validation } from 'itf-validation';
import { yupResolver } from '@hookform/resolvers';
import { FormView } from './form-view';
import { useApi } from '../api-service/useApi';
export var Form = function (_a) {
    var metatype = _a.metatype, apiProps = _a.apiProps, children = _a.children, editId = _a.editId;
    var _b = useApi(createUseFormSets(metatype)), api = _b.api, loading = _b.loading, methods = _b.methods;
    var onSubmit = methods.handleSubmit(function (data) {
        api(__assign({ method: 'post', path: Validation.getFormPath(metatype, 'post'), data: data }, apiProps));
    });
    useEffect(function () {
        if (editId) {
            api({ method: 'get', path: Validation.getFormPath(metatype, 'edit'), onSuccess: methods.reset });
        }
    }, []);
    return React.createElement(FormView, __assign({}, { methods: methods, onSubmit: onSubmit, children: children, loading: loading }));
};
var createUseFormSets = function (metatype) {
    var validationSchema = Validation.getSchema(metatype);
    return { resolver: yupResolver(validationSchema) };
};
