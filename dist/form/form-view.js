var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from 'react';
import { FormProvider } from 'react-hook-form';
import { LoadingBackDrop } from '../backdrop/loading-back-drop';
export var FormView = function (_a) {
    var onSubmit = _a.onSubmit, methods = _a.methods, children = _a.children, loading = _a.loading;
    return (React.createElement(FormProvider, __assign({}, methods),
        React.createElement("form", __assign({}, { onSubmit: onSubmit }), children),
        React.createElement(LoadingBackDrop, __assign({}, { loading: loading }))));
};
